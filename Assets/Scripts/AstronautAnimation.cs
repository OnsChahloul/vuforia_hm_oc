using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AstronautAnimation : MonoBehaviour
{
    private Animator m_Animator;
    private bool drill = false;
    private bool wave = false;
    private bool idle = false;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.D))
        {
            Debug.Log("Launchçing Drilling Animation!");
            wave = false;
            idle = false;
            drill = true;
            m_Animator.SetBool("IsWaving", wave);
            m_Animator.SetBool("IsDrilling", drill);
        }
        if (Input.GetKeyDown(KeyCode.W))
        {
            Debug.Log("Launchçing Waving Animation!");
            wave = true;
            idle = false;
            drill = false;
            m_Animator.SetBool("IsWaving", wave);
            m_Animator.SetBool("IsDrilling", drill);
        }
        if (Input.GetKeyDown(KeyCode.Space))
        {
            Debug.Log("Launchçing Idle Animation!");
            wave = false;
            idle = true;
            drill = false;
            m_Animator.SetBool("IsWaving", wave);
            m_Animator.SetBool("IsDrilling", drill);
        }
    }
}
