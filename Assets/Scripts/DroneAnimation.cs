using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DroneAnimation : MonoBehaviour
{
    private Animator m_Animator;

    private bool scan = false;

    // Start is called before the first frame update
    void Start()
    {
        m_Animator = gameObject.GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
       
        if (Input.GetKeyDown(KeyCode.S))
        {
            Debug.Log("Launchçing scanning Animation!");
          
            scan = true;
           
  
            m_Animator.SetBool("IsScanning", scan);

        }
    }
}
