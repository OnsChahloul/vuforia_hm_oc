using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Menu : MonoBehaviour
{
   
    public GameObject[] food;
    public Text price;
    public Text order; 

    public void openPizza()
    {   for (int i=0; i< food.Length; i++)
        {
            food[i].gameObject.SetActive(false); 
        }
        food[0].gameObject.SetActive(true); //pizza
        price.text = "Price: 15 euro";
    }
    public void openChicken()
    {
        for (int i = 0; i < food.Length; i++)
        {
            food[i].gameObject.SetActive(false);
        }
        food[1].gameObject.SetActive(true); //chicken
        price.text = "Price: 10 euro";

    }
    public void openCoffee()
    {
        for (int i = 0; i < food.Length; i++)
        {
            food[i].gameObject.SetActive(false);
        }
        food[2].gameObject.SetActive(true); //coffee
        price.text = "Price: 5 euro";

    }
    public void openfries()
    {
        for (int i = 0; i < food.Length; i++)
        {
            food[i].gameObject.SetActive(false);
        }
        food[3].gameObject.SetActive(true); //fries
        price.text = "Price: 12 euro";

    }

    public void Order()
    {
        for (int i = 0; i < food.Length; i++)
        {   if (food[i].gameObject.activeSelf == true)
            {
                order.text += food[i].gameObject.name + ", ";
            }
        }
      

    }
}
